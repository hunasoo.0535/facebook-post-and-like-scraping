from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time 
import os
import wget
from bs4 import BeautifulSoup
import requests, re
import sys


url = "https://www.facebook.com/"
options = webdriver.ChromeOptions()
options.add_experimental_option("detach", True)
driver = webdriver.Chrome(options=options)
driver.get(url)

username = driver.find_element(By.NAME, "email")
pwd = driver.find_element(By.NAME, "pass")
login = driver.find_element(By.NAME, "login")
username.send_keys("your email , or login")
pwd.send_keys("your password")
login.click()

#------ get the profile we want to scrap ------#
driver.get("put the url of the facebook profile or page u want to scrap")
time.sleep(3)



HAHA = []
LOVE = []
LIKE = []
ANGRY = []
WOW = []
SAD = []
SOLID = []
NAME = []
TIME = []
CAPTION = []
COMMENT = []

#------- Scroll throught the profile page to load all the data ------#
match=False
while(match==False):
    lenOfPage = driver.execute_script("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
    lastCount = lenOfPage
    time.sleep(10)
    lenOfPage = driver.execute_script("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
    if lastCount==lenOfPage:
        match=True
    soup = BeautifulSoup(driver.page_source, "html.parser")
    all_posts = soup.select("div.du4w35lb.k4urcfbm.l9j0dhe7.sjgh65i0")

#--------- the loaded data had been stored in the var all_posts , now we will loop on every element in the array -------#
for posts in all_posts:
    try :
        name = posts.find("a", {"class" : "oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gpro0wi8 oo9gr5id lrazzd5p"}).get_text()
    except:
        name = "0"
                    
    try :
        tpost = posts.find("a", {"class" : "oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 nc684nl6 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl gmql0nx0 gpro0wi8 b1v8xokw"}).get_text()
    except :
        tpost = "0"
    try:
        caption = posts.find("span", {"class" : "d2edcug0 hpfvmrgz qv66sw1b c1et5uql oi732d6d ik7dh3pa ht8s03o8 a8c37x1j fe6kdd0r mau55g9w c8b282yb keod5gw0 nxhoafnm aigsh9s9 d9wwppkn iv3no6db jq4qci2q a3bd9o3v b1v8xokw oo9gr5id hzawbc8m"}).get_text()
    except:
        caption = "0"
    try:
        all_comments = posts.find("div", {"class" : "oajrlxb2 gs1a9yip mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 nhd2j8a9 mg4g778l pfnyh3mw p7hjln8o tgvbjcpo hpfvmrgz esuyzwwr f1sip0of n00je7tq arfg74bv qs9ysxi8 k77z8yql pq6dq46d btwxx1t3 abiwlrkh lzcic4wl dwo3fsh8 g5ia77u1 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv gmql0nx0 kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h du4w35lb gpro0wi8"}).get_text()
    except:
        all_comments = "0"
        time.sleep(10)
    NAME.append(name)
    TIME.append(tpost)
    CAPTION.append(caption)
    COMMENT.append(all_comments)
time.sleep(5)

#------------ Scrap reactions ------#
#------- Scroll throught the profile page to load all the data ------#

match=False
while(match==False):
    lenOfPage = driver.execute_script("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
    lastCount = lenOfPage
    time.sleep(10)
    lenOfPage = driver.execute_script("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
    if lastCount==lenOfPage:
        match=True
    likes = driver.find_elements(By.XPATH, "//div[contains(@class,'oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of n00je7tq arfg74bv qs9ysxi8 k77z8yql l9j0dhe7 abiwlrkh p8dawk7l lzcic4wl gmql0nx0 ce9h75a5 ni8dbmo4 stjgntxs a8c37x1j')]")
#--------- Scrap all the reactions from the reaction modal ------#
for x in likes:
    driver.execute_script("arguments[0].click( )", x)
    time.sleep(5)
    soup = BeautifulSoup(driver.page_source, "html.parser")
    all_likes = soup.find_all("div", {"class": "l9j0dhe7 du4w35lb cjfnh4rs j83agx80 cbu4d94t lzcic4wl ni8dbmo4 stjgntxs oqq733wu cwj9ozl2 io0zqebd m5lcvass fbipl8qg nwvqtn77 nwpbqux9 iy3k6uwz e9a99x49 g8p4j16d bv25afu3 gc7gaz0o k4urcfbm"})
    for like in all_likes:
        find_hahalike = like.find_all('img',attrs={'src': re.compile("An9yRlv3tqyIsDTiKV0WfMgtabNG9VPyvNiv5USdzPe0Cbp2FdNMvbGH1mvTvI8TczUcd9kED-M5Q1z9-fVK3zAMCRSiYtsWTpWSid0DJlPasg.png")})
        find_lovelike = like.find_all('img',attrs={'src': re.compile("An8KxJw0TdKA0hIHqkw35xWvBGYLLbtgD5y14_K8iN_zaDhCWgixktWzvqA45BTxHACGktnPMx_lkq1uE66153QNE58NZp59iYz6MDdtqgcTZw.png")})
        find_likelike = like.find_all('img',attrs={'src': re.compile("An-tsvy1nZCAfjJDq_e9hwhgJ_ouDg6GOHdVQtc31Lh3B13GEFJ0N3wRI6j2_Lz8icCyU4RkVsKbJckG5NMDv5TxxWie8OqB_kcvCNizVjn7sw.png")})
        find_angrylike = like.find_all('img',attrs={'src': re.compile("An-mj0uPEZ5b6GVy3OC-_ZMV1AGoboZI3SG9P2r3WElt054OlpAmUSq9QPU0i9RdhF07UwCRHIsC06i-w4_VCrnJnBEent1vmcy8MXOQt0msew.png")})
        find_wowlike = like.find_all('img',attrs={'src': re.compile("An_f5KryEO3JdbkuRbEs1ixj8HC8itKTXvZ3Hl1c-zaREaiMDPCRTNw6CSwRUjKkq_YXEuxmsqBu06WIeteZ7MBZ2WKuJXvOK6WdOQfGi2Ixg9Sd.png")})
        find_sadlike = like.find_all('img',attrs={'src': re.compile("An9Yzzh8CoEGqeWIfY5w6zR3VdPbG5X1fHXZdMfftnoomx3ObysBj145G99ZhM1T6DcU_ZAH2bEdiOj8sUAQvplVo0cYKS_GprBBJlcwiBHomFx7hQ.png")})
        find_solidlike = like.find_all('img',attrs={'src': re.compile("An-zbifsrGonJXBikRGgj1txFJUkRG3aCN5900mzKo6dVL8tKCuWwF6D9Ov6XB3JJZ7pT1FSxuFsETOjkjZ08b5AyPU0z_GsxZH2nWiW2ScJT4p3rQ.png")})                                                          

        if find_hahalike:
            for x in find_hahalike[1:2]:
                num_haha = x.find_parent("div", {"class" : "bp9cbjyn rq0escxv j83agx80 pfnyh3mw l9j0dhe7 cehpxlet aodizinl hv4rvrfc ofv0k9yr dati1w0a"}).find("span", {"class" : re.compile("d2edcug0 hpfvmrgz qv66sw1b c1et5uql oi732d6d ik7dh3pa ht8s03o8 a8c37x1j fe6kdd0r mau55g9w c8b282yb keod5gw0 nxhoafnm aigsh9s9 d9wwppkn iv3no6db jq4qci2q a3bd9o3v lrazzd5p")}).get_text()
        else:
            num_haha = "0"
        if find_lovelike :
            for x in find_lovelike[1:2]:
                num_love = x.find_parent("div", {"class" :"bp9cbjyn rq0escxv j83agx80 pfnyh3mw l9j0dhe7 cehpxlet aodizinl hv4rvrfc ofv0k9yr dati1w0a"}).find("span", {"class" : re.compile("d2edcug0 hpfvmrgz qv66sw1b c1et5uql oi732d6d ik7dh3pa ht8s03o8 a8c37x1j fe6kdd0r mau55g9w c8b282yb keod5gw0 nxhoafnm aigsh9s9 d9wwppkn iv3no6db jq4qci2q a3bd9o3v lrazzd5p")}).get_text()
        else:
            num_love = "0"
                
        if find_likelike:
            for x in find_likelike[1:2]:
                num_like = x.find_parent("div", {"class" : "bp9cbjyn rq0escxv j83agx80 pfnyh3mw l9j0dhe7 cehpxlet aodizinl hv4rvrfc ofv0k9yr dati1w0a"}).find('span', {"class" : re.compile("d2edcug0 hpfvmrgz qv66sw1b c1et5uql oi732d6d ik7dh3pa ht8s03o8 a8c37x1j fe6kdd0r mau55g9w c8b282yb keod5gw0 nxhoafnm aigsh9s9 d9wwppkn iv3no6db jq4qci2q a3bd9o3v lrazzd5p")}).get_text()
        else:
            num_like = "0"
        if find_angrylike:
            for x in find_angrylike[1:2]:
                num_angry = x.find_parent("div", {"class" : "bp9cbjyn rq0escxv j83agx80 pfnyh3mw l9j0dhe7 cehpxlet aodizinl hv4rvrfc ofv0k9yr dati1w0a"}).find('span', {"class" : re.compile("d2edcug0 hpfvmrgz qv66sw1b c1et5uql oi732d6d ik7dh3pa ht8s03o8 a8c37x1j fe6kdd0r mau55g9w c8b282yb keod5gw0 nxhoafnm aigsh9s9 d9wwppkn iv3no6db jq4qci2q a3bd9o3v lrazzd5p")}).get_text()
        else:
            num_angry = "0"
        if find_wowlike:
            for x in find_wowlike[1:2]:
                num_wow = x.find_parent("div", {"class" : "bp9cbjyn rq0escxv j83agx80 pfnyh3mw l9j0dhe7 cehpxlet aodizinl hv4rvrfc ofv0k9yr dati1w0a"}).find('span', {"class" : re.compile("d2edcug0 hpfvmrgz qv66sw1b c1et5uql oi732d6d ik7dh3pa ht8s03o8 a8c37x1j fe6kdd0r mau55g9w c8b282yb keod5gw0 nxhoafnm aigsh9s9 d9wwppkn iv3no6db jq4qci2q a3bd9o3v lrazzd5p")}).get_text()
        else:
            num_wow = "0"
        if find_sadlike:
            for x in find_sadlike[1:2]:
                num_sad = x.find_parent("div", {"class" : "bp9cbjyn rq0escxv j83agx80 pfnyh3mw l9j0dhe7 cehpxlet aodizinl hv4rvrfc ofv0k9yr dati1w0a"}).find('span', {"class" : re.compile("d2edcug0 hpfvmrgz qv66sw1b c1et5uql oi732d6d ik7dh3pa ht8s03o8 a8c37x1j fe6kdd0r mau55g9w c8b282yb keod5gw0 nxhoafnm aigsh9s9 d9wwppkn iv3no6db jq4qci2q a3bd9o3v lrazzd5p")}).get_text()
        else:
            num_sad = "0"
        if find_solidlike:
            for x in find_solidlike[1:2]:
                num_solid = x.find_parent("div", {"class" : "bp9cbjyn rq0escxv j83agx80 pfnyh3mw l9j0dhe7 cehpxlet aodizinl hv4rvrfc ofv0k9yr dati1w0a"}).find('span', {"class" : re.compile("d2edcug0 hpfvmrgz qv66sw1b c1et5uql oi732d6d ik7dh3pa ht8s03o8 a8c37x1j fe6kdd0r mau55g9w c8b282yb keod5gw0 nxhoafnm aigsh9s9 d9wwppkn iv3no6db jq4qci2q a3bd9o3v lrazzd5p")}).get_text()
        else:
            num_solid = "0"
        time.sleep(6)            

    close = driver.find_element(By.XPATH, "//div[contains(@aria-label,'Fermer')]")
    driver.execute_script("arguments[0].click( )", close)
    time.sleep(5)
    HAHA.append(num_haha)
    LOVE.append(num_love)
    LIKE.append(num_like)
    ANGRY.append(num_angry)
    WOW.append(num_wow)
    SAD.append(num_sad)
    SOLID.append(num_solid)
  

#------- load data to store it in csv format file --------#
import pandas as pd

data = {
"NAME": NAME,
  "TIME": TIME,
  "CAPTION" : CAPTION,
  "COMMENT": COMMENT,
"HAHA" : HAHA,
"LOVE" : LOVE,
"LIKE" : LIKE,
"ANGRY" : ANGRY,
"WOW" : WOW,
"SAD" : SAD,
"SOLID" : SOLID

}

#load data into a DataFrame object:
df = pd.DataFrame(data)

df.to_csv("facebook_like.csv", encoding="utf-8") 

"""
#--------- Scrap Profile images --------#

images =[]
for i in ['photos_by']:
    driver.get("https://www.facebook.com/blocklady99/" + i + "/")
    n_scrools = 3
    for j in range(1, n_scrools):
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        anchors = driver.find_elements(By.TAG_NAME, "a")
        anchors = [a.get_attribute('href') for a in anchors]
        anchors = [a for a in anchors if str(a).startswith("https://www.facebook.com/photo")]
    for a in anchors:
        driver.get(a)
        time.sleep(5)
        img = driver.find_elements(By.TAG_NAME, "img")
        images.append(img[0].get_attribute("src"))

pc = os.getcwd()
pc = os.path.join(pc, "scrappath")

os.mkdir(pc)

sounter = 1
for image in images:
    save_as = os.path.join(pc, str(sounter) + ".jpg")
    wget.download(image, save_as)
    sounter+= 1

time.sleep(10)
#--------- Scrap friend list ------#
for x in ['friends_recent']:
    driver.get("https://www.facebook.com/blocklady99/" + x + "/")
    mm = 3
    for y in range(1, mm):
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        ami = driver.find_elements(By.TAG_NAME, "a")
        ami = [d.get_attribute('href') for d in ami ]
        ami = [d for d in ami if str(d).startswith("https://www.facebook.com/")]
        print(ami)

d = driver.get("https://www.facebook.com/blocklady99")


     """



